# FedEx_Installer Module

The FedEx_Installer module provides functionality to help adds a list of attributes and cms pages/blocks from a file

## Dependencies

You can find a list of modules in the require section of the `composer.json` file located in the
same directory as this `README.md` file.